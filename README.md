#Elasticsearch Effigy helper

##General information
We decided to use [**elasticsearch**](https://www.elastic.co/) as our search engine primary for storing numeric vectors of image features with some 
additional metadata and performing search operations on them. 

By default elasticsearch does not include search on numeric vectors. However,
It supports custom plugins. That is why we decided to write one, which would help us with search on vectors, ourselves.

The resulting plugin is running inside elasticsearch. It is able to perform search on numeric vectors and extensively uses [LIRE](http://www.lire-project.net/) library for measuring the distance between vectors of image features.
To make a search request using our plugin as a scorer, there exist special type of query which is called [function score query](https://www.elastic.co/guide/en/elasticsearch/reference/5.1/query-dsl-function-score-query.html).

##Usage
Target version of elasticsearch for our plugin is [5.1.1](https://www.elastic.co/downloads/past-releases/elasticsearch-5-1-1)

Steps to build the plugin and add it to elasticsearch:

* Download and unzip elasticsearch from the official website
* Download/clone plugin repository
* Open terminal and proceed to the directory where plugin repository is unzipped / cloned, build plugin using the following command:
 
 > $ gradle plugin 
 
* Or alternatively if you do not have [gradle](https://gradle.org/) installed u can use gradle wrapper:

>$ ./gradlew plugin

* Next proceed to the `build/distributions` directory. There was created file `effigy-helper-1.0.zip`, please unzip it and copy the folder `effigy-helper-1.0` to your elasticsearch `plugins` directory.
* Now start your elasticsearch and plugin will be loaded on startup.