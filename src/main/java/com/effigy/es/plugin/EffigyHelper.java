package com.effigy.es.plugin;


import com.effigy.es.service.CustomScorer;
import org.apache.lucene.util.BytesRef;
import org.elasticsearch.common.collect.HppcMaps;
import org.elasticsearch.common.inject.internal.Nullable;
import org.elasticsearch.index.fielddata.ScriptDocValues;
import org.elasticsearch.index.get.GetField;
import org.elasticsearch.plugins.Plugin;
import org.elasticsearch.plugins.ScriptPlugin;
import org.elasticsearch.script.AbstractDoubleSearchScript;
import org.elasticsearch.script.ExecutableScript;
import org.elasticsearch.script.NativeScriptFactory;

import java.util.*;

/**
 * Created by Borys on 12/30/16.
 */
public class EffigyHelper extends Plugin implements ScriptPlugin{

    @Override
    public List<NativeScriptFactory> getNativeScripts() {
        return Collections.singletonList(new MyNativeScriptFactory());
    }

    public static class MyNativeScriptFactory implements NativeScriptFactory {

        private final String SCRIPT_NAME = "effigy-helper";
        private final CustomScorer service;

        public MyNativeScriptFactory(){
            this.service = new CustomScorer();
        }

        @Override
        public ExecutableScript newScript(@Nullable Map<String, Object> params) {
            return new AbstractDoubleSearchScript() {
                /*
                 * called for every filtered document
                 */
                @Override
                public double runAsDouble() {
                    //TODO Set custom scorer parameters
                    if((params.containsKey("feature_name"))&&(params.containsKey("feature_value"))){
                        String featureName = (String) params.get("feature_name");
                        List<Integer> featureValue = (List<Integer>) params.get("feature_value");

                        Object tmp  = source().get(featureName);
                        List<Integer> docValue = null;
                        if(tmp  instanceof List){
//                            System.out.println("1 - true | [" + featureName + "] For image with ID " + id);
                            docValue = (List<Integer>) tmp;
                            return service.score(featureName, docValue, featureValue);
                        }
                        else return service.dummyScore();
//                        System.out.println("Class - " + docValue.get(0).getClass()+ "For image with ID " + id);
                    }
                    else return service.dummyScore();
                }
            };
        }
        @Override
        public boolean needsScores() {
            return false;
        }

        @Override
        public String getName() {
            return SCRIPT_NAME;
        }
    }

}
