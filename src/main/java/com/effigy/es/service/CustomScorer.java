package com.effigy.es.service;

import com.effigy.es.data.Features;
import net.semanticmetadata.lire.imageanalysis.features.LireFeature;

import java.util.List;

/**
 * Created by Borys on 12/31/16.
 */
public class CustomScorer {


    public CustomScorer(){

    }
    public double score(String featureName, List<Integer> docValue, List<Integer> featureValue){
        double result;
        LireFeature givenFeature = null, docFeature = null;
        Features featureEnum = Features.getByName(featureName);
        try {
            givenFeature = featureEnum.getFeatureClass().newInstance();
            docFeature = featureEnum.getFeatureClass().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
//        System.out.println(featureName);
        assert givenFeature != null;
        givenFeature.setByteArrayRepresentation(toByteArray(featureValue));
        assert docFeature != null;
        docFeature.setByteArrayRepresentation(toByteArray(docValue));

        double distance = givenFeature.getDistance(docFeature);
        if (Double.compare(distance, 1.0f) <= 0) { // distance less than 1, consider as same image
            result = 2f - distance;
        } else {
            result = 1 / distance;
        }
        return result;
    }
    private byte[] toByteArray(List<Integer> list){
        byte[] arr = new byte[list.size()];
        for(int i=0;i<arr.length;i++)
        {
            arr[i] = list.get(i).byteValue();
        }
        return arr;
    }

    public double dummyScore() {
        return -1;
    }
}
